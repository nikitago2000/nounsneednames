module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'usm': '380px',
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    fontFamily: {
      'sans': ['Inter'],
      // 'sans': ['PT Root UI'],
      'mono': ['Space Mono'],
      'display': ['Londrina Solid'],
      'noun': ['Noun Named'],
    },
    extend: {},
    colors: {
      transparent: 'transparent',
      white: {
        DEFAULT: '#FFFFFF',
        dark: '#EBE8E3',
        app: '#F2F2F2'
      },
      black: {
        DEFAULT: "#000000",
        text: "#212529"
      },
      warm: "#E1D7D5",
      cool: "#d5d7e1",
      branding: {
        purple: '#5A65FA',
      },
      green: {
        DEFAULT: '#42FF02',
        select: '#4dc175',
      },
      red: "#d22208",
      grey: {
        light: '#9D9D9D',
        DEFAULT: '#7D7D7D',
        dark: '#616161'
      },
      brand: {
        twitter: "#1da1f2",
        instagram: "#e1306c",
        facebook: "#4267b2"
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
  ],
}
