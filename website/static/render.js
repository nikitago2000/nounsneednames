import { createCanvas } from "canvas";

export function drawNoun(noun) {
    const width = 1380;
    const height = 1380;

    const canvas = createCanvas(width, height);
    const context = canvas.getContext("2d");

    if (noun.seedMap.background == 0) {
        context.fillStyle = "#d5d7e1";
    } else {
        context.fillStyle = "#E1D7D5";
    }
    context.fillRect(0, 0, width, height);

    context.font = "150px Roboto";
    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = noun.textColor;

    const text = noun.name;

    const textWidth = context.measureText(text).width;
    // context.fillRect(600 - textWidth / 2 - 10, 170 - 5, textWidth + 20, 120);

    context.fillText(text, 600, 170);

    var image = noun.svg;

    // context.drawImage(image, 340, 515, 70, 70);

    return '<img src="' + canvas.toDataURL() + '" />';

    // const buffer = canvas.toBuffer("image/png");
    // fs.writeFileSync("./test.png", buffer);
}