
// Data taken from https://nouns.com/assets-list.
// Since id there start not from 0 but 1, there is
// a need to increase seed number, so id's would
// match correctly with ones stored on chain
const accessoryColorMap = {
    17: "#000000",
    18: "#552e04",
    19: "#fe3a0e",
    20: "#667af9",
    21: "#70bde4",
    22: "#9f21a0",
    23: "#844398",
    24: "#bd2d23",
    27: "#254efb",
    33: "#2a83f6",
    44: "#505a5c",
    45: "#505a5c",
    72: "#5e6061",
    89: "#000000",
    134: "#c5b9a2",
    135: "#fffdf2",
    136: "#1f1d29",
    137: "#caeff9",
}

export function colorForTheName(seed) {
    if (accessoryColorMap.hasOwnProperty(seed.accessory + 1)) {
        return accessoryColorMap[seed.accessory + 1];
    } else return null;
}