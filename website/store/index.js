import data from '/assets/data/nouns.json';

export const actions = {
    async nuxtServerInit({ commit }) {
        // let file = await require(
        //     "/assets/names/data_wip.json",
        // );
        // console.log(data[0]);
        // let nouns = JSON.parse(data);
        await commit("setNouns", data);
    },
    CHANGE_SELECTED_NOUN({ state, dispatch, commit }) {
        const winCheck = isWin(state.boardState);
        if (winCheck.result) {
            commit("endGame", winCheck);
        }
        if (isDraw(state.boardState, state.history)) {
            commit("endGame", { who: 'tie' });
        }
        if (state.currentPlayer === CPU) {
            dispatch("CPU_NEXT_MOVE");
        }
    },
};

export const mutations = {
    togglePfpPreview(state) {
        state.pfpPreview = !state.pfpPreview;
    },
    setNouns(state, list) {
        state.nouns = list.sort(function (a, b) {
            return a.id - b.id;
        });
    }
};

export const state = () => ({
    pfpPreview: false,
    nouns: []
});