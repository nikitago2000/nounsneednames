var fs = require('fs');

async function mergeNounsWithNames() {
    let nounsDataRawData = fs.readFileSync('../scripts/data/nouns_data.json');
    const nounsData = JSON.parse(nounsDataRawData);

    let namedNounsRawData = fs.readFileSync('../scripts/data/named_nouns.json');
    const namedNouns = JSON.parse(namedNounsRawData);

    let nouns = []

    nounsData.forEach(e => {
        let nameData = namedNouns.find(n => { return n.id == e.id });
        if (nameData != undefined) {
            e.name = nameData.name;
            e.layout = nameData.layout;
        }

        nouns.push(e);
    });

    var jsonData = JSON.stringify(nouns);

    fs.writeFile("data/nouns.json", jsonData, function (err) {
        if (err) {
            console.log(err);
        }
    });
}

module.exports.mergeNounsWithNames = mergeNounsWithNames;

mergeNounsWithNames()