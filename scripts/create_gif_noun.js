const nouns = require('../scripts/data/nouns.json');
const GIFEncoder = require('gifencoder');
const { createCanvas, loadImage } = require('canvas');
const fs = require('fs');

//TODO: make font register for use on OS without font preinstalled
// const { registerFont } = require('canvas')
// registerFont('comicsans.ttf', { family: 'Comic Sans' })

/**
* @param {int} id What Noun to load for the GIF creation
*/
async function createGifNoun(id, animationType) {
    const nounId = parseInt(id);

    var noun = nouns.find(x => x.id === nounId);

    if (noun === undefined) {
        console.log(`Noun with the id: ${id} not found in the /data/nouns.json file. Exit.`)
        return;
    } else {
        console.log("Gif creation started.")
    }

    const width = 1380;
    const height = 1380;

    const encoder = new GIFEncoder(width, height);

    const fileName = "outputs/noun " + nounId + " animated.gif"
    // stream the results as they are available into gif
    encoder.createReadStream().pipe(fs.createWriteStream(fileName));

    encoder.start();
    encoder.setRepeat(0);   // 0 for repeat, -1 for no-repeat
    encoder.setQuality(10); // image quality. 10 is default.

    // use node-canvas
    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    // ------------------ IMAGE PREP ------------------

    let prepImg = Buffer.from(noun.svg, "base64").toString();
    prepImg = prepImg.replace('width="320"', 'width="960"');
    prepImg = prepImg.replace('height="320"', 'height="960"');

    const preppedImg = Buffer.from(prepImg).toString("base64");

    var textColor = prepImg.slice(217, 224);
    noun.textColor = textColor;

    // ------------------ TEXT DATA ------------------

    ctx.font = "300px Noun Named";
    ctx.textBaseline = "middle";
    ctx.fillStyle = noun.textColor;

    const text = noun.name;

    const textWidth = ctx.measureText(text).width;
    const textX = (width - textWidth) / 2;

    // ------------------ FUNCTIONS ------------------

    // --- util functions ---

    function drawBackground() {
        if (noun.seedMap.background == 0) {
            ctx.fillStyle = "#d5d7e1";
        } else {
            ctx.fillStyle = "#E1D7D5";
        }
        ctx.fillRect(0, 0, width, height);
    }

    function drawRect(x, y, w, h, color) {
        ctx.fillStyle = color;
        ctx.fillRect(x, y, w, h);
    }

    function drawText() {
        ctx.font = "300px Noun Named";
        ctx.textBaseline = "middle";
        ctx.fillStyle = noun.textColor;
        ctx.fillText(text, (width - textWidth) / 2, 1125);
    }

    // --- draw functions ---

    function drawRectGrowFrame(img, rectY, rectH) {
        drawBackground();
        ctx.drawImage(img, 210, 30);
        drawRect(textX + 15, rectY, textWidth - 30, rectH, noun.textColor);
        encoder.setDelay(50);
        encoder.addFrame(ctx);
    }

    function drawNameRevealFrame(img, rect1Y, rect2Y, rectH) {
        drawBackground();
        ctx.drawImage(img, 210, 30);

        drawText();

        drawRect(textX + 15, rect1Y, textWidth - 30, rectH, noun.textColor);
        drawRect(textX + 15, rect2Y, textWidth - 30, rectH, noun.textColor);
        encoder.setDelay(75);
        encoder.addFrame(ctx);
    }

    function drawBasicFrame(img, delay = 100) {
        drawBackground();
        ctx.drawImage(img, 210, 30);

        drawText();

        encoder.setDelay(delay);
        encoder.addFrame(ctx);
    }

    function drawEyesSemiClosed(img, delay = 100) {
        drawBackground();
        ctx.drawImage(img, 210, 30);

        drawRect(540, 390, 120, 120, '#ffffff');
        drawRect(750, 390, 120, 120, '#ffffff');

        drawRect(600, 420, 60, 60, '#000000');
        drawRect(810, 420, 60, 60, '#000000');
        // drawRect(600, 420, 60, 60, '#fe0d0e');
        // drawRect(810, 420, 60, 60, '#fe0d0e');

        drawText();

        encoder.setDelay(delay);
        encoder.addFrame(ctx);
    }

    function drawEyesClosed(img, delay = 100) {
        drawBackground();
        ctx.drawImage(img, 210, 30);

        drawRect(540, 390, 120, 120, '#ffffff');
        drawRect(750, 390, 120, 120, '#ffffff');

        drawText();

        encoder.setDelay(delay);
        encoder.addFrame(ctx);
    }

    function drawEyesLookingLeft(img, delay = 100) {
        drawBackground();
        ctx.drawImage(img, 210, 30);

        drawRect(540, 390, 120, 120, '#ffffff');
        drawRect(750, 390, 120, 120, '#ffffff');

        drawRect(540, 390, 60, 120, '#000000');
        drawRect(750, 390, 60, 120, '#000000');

        drawText();

        encoder.setDelay(delay);
        encoder.addFrame(ctx);
    }

    // --- different animation creation functions ---

    function brickNameRevealAnimation(img) {
        // --------- FRAME 1 ---------
        drawRectGrowFrame(img, 1100, 50);

        // --------- FRAME 2 ---------
        drawRectGrowFrame(img, 1087, 75);

        // --------- FRAME 3 ---------
        drawRectGrowFrame(img, 1075, 100);

        // --------- FRAME 4 ---------
        drawRectGrowFrame(img, 1062, 125);

        // --------- FRAME 5 ---------
        drawRectGrowFrame(img, 1050, 150);

        // --------- FRAME 6 ---------
        drawNameRevealFrame(img, 1050, 1150, 50);

        // --------- FRAME 7 ---------
        drawNameRevealFrame(img, 1050, 1175, 25);

        // --------- FRAME 8 ---------
        drawBasicFrame(img, 500);
    }

    function eyesCloseAnimation(img) {
        // --------- FRAME 1 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 2 ---------
        drawEyesClosed(img, 40);

        // --------- FRAME 3 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 4 ---------
        drawBasicFrame(img, 250);

        // --------- FRAME 5 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 6 ---------
        drawEyesClosed(img, 40);

        // --------- FRAME 7 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 8 ---------
        drawBasicFrame(img, 1000);
    }

    function lookLeftRightAnimation(img) {
        // --------- FRAME 1 ---------
        drawEyesLookingLeft(img, 350);

        // --------- FRAME 2 ---------
        drawBasicFrame(img, 600);

        // --------- FRAME 3 ---------
        drawEyesLookingLeft(img, 350);

        // --------- FRAME 4 ---------
        drawBasicFrame(img, 1500);
        // drawBasicFrame(img, 1000);
    }

    function lookLeftRightEyesCloseAnimation(img) {
        // --------- FRAME 1 ---------
        drawEyesLookingLeft(img, 350);

        // --------- FRAME 2 ---------
        drawBasicFrame(img, 600);

        // --------- FRAME 3 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 4 ---------
        drawEyesClosed(img, 40);

        // --------- FRAME 5 ---------
        drawEyesSemiClosed(img, 75);

        // --------- FRAME 6 ---------
        drawBasicFrame(img, 1000);
    }

    function eyesOnceCloseLongAnimation(img) {
        // --------- FRAME 1 ---------
        drawEyesSemiClosed(img, 50);

        // --------- FRAME 2 ---------
        drawEyesClosed(img, 800);

        // --------- FRAME 3 ---------
        drawEyesSemiClosed(img, 65);

        // --------- FRAME 4 ---------
        drawBasicFrame(img, 1500);
    }

    // ------------------ WORK PART ------------------

    if (animationType === "lookLeftRight") {
        loadImage(`data:image/svg+xml;base64,${preppedImg}`).then(img => {
            brickNameRevealAnimation(img);

            lookLeftRightAnimation(img);

            encoder.finish();

            console.log("Gif creation finished.")
        });
    } else if (animationType === "nameOnly") {
        loadImage(`data:image/svg+xml;base64,${preppedImg}`).then(img => {
            brickNameRevealAnimation(img);

            drawBasicFrame(img, 1000);

            encoder.finish();

            console.log("Gif creation finished.")
        });
    } else if (animationType === "eyesOnceCloseLong") {
        loadImage(`data:image/svg+xml;base64,${preppedImg}`).then(img => {
            brickNameRevealAnimation(img);

            eyesOnceCloseLongAnimation(img);

            encoder.finish();

            console.log("Gif creation finished.")
        });
    } else if (animationType === "lookLeftRightEyesClose") {
        loadImage(`data:image/svg+xml;base64,${preppedImg}`).then(img => {
            brickNameRevealAnimation(img);

            lookLeftRightEyesCloseAnimation(img);

            encoder.finish();

            console.log("Gif creation finished.")
        });
    } else {
        loadImage(`data:image/svg+xml;base64,${preppedImg}`).then(img => {
            brickNameRevealAnimation(img);

            eyesCloseAnimation(img);

            encoder.finish();

            console.log("Gif creation finished.")
        });
    }
}

// Animation types:
// - eyesClose
// - eyesOnceCloseLong
// - lookLeftRight
// - lookLeftRightEyesClose
// - nameOnly

module.exports.createGifNoun = createGifNoun;

createGifNoun(197, "lookLeftRightEyesClose")