const NounsDescriptorAbi = require("../scripts/data/abis/NounsDescriptor.json")
const NounsTokenAbi = require("../scripts/data/abis/NounsToken.json")
const { ethers } = require("ethers");

// const API_URL = process.env.API_URL

var fs = require('fs');

async function downloadNouns() {
    const provider = new ethers.providers.JsonRpcProvider("https://eth-mainnet.alchemyapi.io/v2/2FGDon9vA0u-9Nxn_vXY9_a1UvBuiVpo");

    const addresses = {
        nounsDescriptor: "0x0Cfdb3Ba1694c2bb2CFACB0339ad7b1Ae5932B63",
        nounsToken: "0x9C8fF314C9Bc7F6e59A9d9225Fb22946427eDC03",
    };

    const contracts = {
        nounsDescriptor: new ethers.Contract(
            addresses.nounsDescriptor,
            NounsDescriptorAbi,
            provider
        ),
        nounsToken: new ethers.Contract(addresses.nounsToken, NounsTokenAbi, provider),
    };

    const totalSupply = await contracts.nounsToken.totalSupply();
    const lastNoun = parseInt(totalSupply._hex, 16) - 1;

    let nouns = [];

    for (let index = 0; index <= lastNoun; index++) {
        const id = index;

        const seed = await contracts.nounsToken.seeds(id);
        const svg = await contracts.nounsDescriptor.generateSVGImage(seed);

        const seedMap = {
            background: seed[0],
            body: seed[1],
            accessory: seed[2],
            head: seed[3],
            glasses: seed[4],
        };

        const noun = {
            id: id,
            name: "WIP",
            layout: "wip",
            seed: seed,
            seedMap: seedMap,
            svg: svg,
        };

        nouns.push(noun);
    }

    var jsonData = JSON.stringify(nouns);

    fs.writeFile("data/nouns_data.json", jsonData, function (err) {
        if (err) {
            console.log(err);
        }
    });
}

module.exports.downloadNouns = downloadNouns;

downloadNouns()