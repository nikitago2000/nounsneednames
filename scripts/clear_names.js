var fs = require('fs');

let nouns = []

let nounsToGenerate = 160

for (let index = 0; index < nounsToGenerate; index++) {

    let noun = {
        "id": index,
        "name": "Wip",
        "layout": "bottom-1"
    }
    nouns.push(noun);

}

var jsonData = JSON.stringify(nouns);

fs.writeFile("data/named_nouns_clear.json", jsonData, function (err) {
    if (err) {
        console.log(err);
    }
});